/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_HRTIM_MspPostInit(HRTIM_HandleTypeDef *hhrtim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SENS1_RST_Pin GPIO_PIN_13
#define SENS1_RST_GPIO_Port GPIOC
#define RESET_Pin GPIO_PIN_10
#define RESET_GPIO_Port GPIOG
#define SENS1_INT_Pin GPIO_PIN_0
#define SENS1_INT_GPIO_Port GPIOC
#define SENS2_INT_Pin GPIO_PIN_1
#define SENS2_INT_GPIO_Port GPIOC
#define SENS4_ANALOG_Pin GPIO_PIN_2
#define SENS4_ANALOG_GPIO_Port GPIOC
#define SENS3_ANALOG_Pin GPIO_PIN_3
#define SENS3_ANALOG_GPIO_Port GPIOC
#define SENS2_ANALOG_Pin GPIO_PIN_0
#define SENS2_ANALOG_GPIO_Port GPIOA
#define SENS1_ANALOG_Pin GPIO_PIN_1
#define SENS1_ANALOG_GPIO_Port GPIOA
#define SENS1_TX_Pin GPIO_PIN_2
#define SENS1_TX_GPIO_Port GPIOA
#define SENS1_RX_Pin GPIO_PIN_3
#define SENS1_RX_GPIO_Port GPIOA
#define SENS3_4_CS_Pin GPIO_PIN_4
#define SENS3_4_CS_GPIO_Port GPIOA
#define SENS1_SCK_Pin GPIO_PIN_5
#define SENS1_SCK_GPIO_Port GPIOA
#define SENS1_MISO_Pin GPIO_PIN_6
#define SENS1_MISO_GPIO_Port GPIOA
#define SENS1_MOSI_Pin GPIO_PIN_7
#define SENS1_MOSI_GPIO_Port GPIOA
#define SENS2_RX_Pin GPIO_PIN_5
#define SENS2_RX_GPIO_Port GPIOC
#define LED_G_Pin GPIO_PIN_0
#define LED_G_GPIO_Port GPIOB
#define LED_R_Pin GPIO_PIN_1
#define LED_R_GPIO_Port GPIOB
#define SENS3_INT_Pin GPIO_PIN_2
#define SENS3_INT_GPIO_Port GPIOB
#define SENS3_TX_Pin GPIO_PIN_10
#define SENS3_TX_GPIO_Port GPIOB
#define SENS3_RX_Pin GPIO_PIN_11
#define SENS3_RX_GPIO_Port GPIOB
#define SENS2_CS_Pin GPIO_PIN_12
#define SENS2_CS_GPIO_Port GPIOB
#define SENS2_SCK_Pin GPIO_PIN_13
#define SENS2_SCK_GPIO_Port GPIOB
#define SENS2_MISO_Pin GPIO_PIN_14
#define SENS2_MISO_GPIO_Port GPIOB
#define SENS2_MOSI_Pin GPIO_PIN_15
#define SENS2_MOSI_GPIO_Port GPIOB
#define SENS1_PWM_Pin GPIO_PIN_6
#define SENS1_PWM_GPIO_Port GPIOC
#define SENS2_PWM_Pin GPIO_PIN_7
#define SENS2_PWM_GPIO_Port GPIOC
#define SENS3_PWM_Pin GPIO_PIN_8
#define SENS3_PWM_GPIO_Port GPIOC
#define SENS4_PWM_Pin GPIO_PIN_9
#define SENS4_PWM_GPIO_Port GPIOC
#define SENS2_TX_Pin GPIO_PIN_9
#define SENS2_TX_GPIO_Port GPIOA
#define SENS2_RST_Pin GPIO_PIN_10
#define SENS2_RST_GPIO_Port GPIOA
#define SENS1_CS_Pin GPIO_PIN_15
#define SENS1_CS_GPIO_Port GPIOA
#define DEBUG_TX_Pin GPIO_PIN_10
#define DEBUG_TX_GPIO_Port GPIOC
#define DEBUG_RX_Pin GPIO_PIN_11
#define DEBUG_RX_GPIO_Port GPIOC
#define SENS4_RX_Pin GPIO_PIN_2
#define SENS4_RX_GPIO_Port GPIOD
#define SENS3_4_SCK_Pin GPIO_PIN_3
#define SENS3_4_SCK_GPIO_Port GPIOB
#define SENS3_4_MISO_Pin GPIO_PIN_4
#define SENS3_4_MISO_GPIO_Port GPIOB
#define SENS3_4_MOSI_Pin GPIO_PIN_5
#define SENS3_4_MOSI_GPIO_Port GPIOB
#define SENS4_INT_Pin GPIO_PIN_6
#define SENS4_INT_GPIO_Port GPIOB
#define SENS3_RST_Pin GPIO_PIN_7
#define SENS3_RST_GPIO_Port GPIOB
#define SENS4_RST_Pin GPIO_PIN_9
#define SENS4_RST_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
