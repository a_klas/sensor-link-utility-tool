# Sensor Link Utility Tool - SLUT

**Platforma edukacyjna zbudowana w oparciu o standard mikroBUS firmy MikroElektronika**

|Schemat blokowy| MOTHERBOARD TOP| MOTHERBOARD BOTTOM |
----|----|----
|![plot](./images/block_diagram.png) |![plot](./images/motherboard_top.png) | ![plot](./images/motherboard_bottom.png)|

## 1. Co zawiera repozytorium? 
W repozytorium znajdują się wszystkie pliki potrzebne do rozpoczęcia przygody z platformą. Poszczególne foldery odpowiadają za:

- FIRMWARE - zawiera wzór oprogramowania gotowego do wgrania na mikroprocesor (struktura projektu, docker) [SOON];
- HARDWARE - zawiera projekt płyty PCB MOTHERBOARD (schematy, pcb oraz pliki gerber);
- SOFTWARE - zawiera oprogramowanie z którego korzystamy do testowania wszystkich modułów [SOON].

Dodatkowo do repozytorium podpięte są submoduły, zawierają wzory potrzebne do zaprojektowania własnego modułu mikroBus [ ./SENSORS] oraz gotowe moduły mikroBus [np. ./SENSORS/BME280].

## 2. Po co to wszystko?
Chcemy, aby to repozytorium zawierało wszystko czego studenci mogą potrzebować na zajęciach, łącznie z wzorem projektu 
oprogramowania. Pozwoli to na uniknięcie możliwych błędów np:

- związanych z połączeniami za pomocą kabelków;
- związanych z środowiskiem progamistycznym;

Wszystko sprowadza się do tego, że unikamy stwierdzenia "U mnie nie działa, co mam zrobić?" i umożliwiamy studentom skumienie się na 
celu zajęć (np. budowę systemu).

## 3. Jak pobrać repozytorium?
Aby pobrać repozytorium należy w terminalu (np. PowerShell w Windowsie) wprowadzić komende:

     - git clone --recurse-submodules https://gitlab.com/wsn_agh/slut

Pozwoli to na pobranie całego repozytorium łącznie z submodułami.
